FROM debian:bookworm
LABEL maintainer "Stefan"

RUN set -eu && \
	apt-get update && apt-get install --no-install-recommends -y \
	ca-certificates \
	curl \
	pipewire-audio \
	pipewire-jack \
	libspa-0.2-jack \
	ardour \
	calf-plugins \
	fluid-soundfont-gm \
	lsp-plugins-lv2 \
	x42-plugins \
	zynaddsubfx-lv2 \
	libxcb-cursor0 \
	libxcb-xkb1 \
	libxkbcommon-x11-0 \
	xdg-utils \
	xclip \
	zenity && \
	curl -ORL https://github.com/surge-synthesizer/releases-xt/releases/download/1.3.4/surge-xt-linux-x64-1.3.4.deb && \
	curl -ORL https://download.opensuse.org/repositories/home:/sfztools:/sfizz/Debian_12/amd64/sfizz_1.2.3-0_amd64.deb && \
	curl -ORL https://github.com/Geonkick-Synthesizer/geonkick/releases/download/v3.4.0/GNULinux_binaries_v3.4.0.tar.gz && \
	dpkg -i *.deb && \
	tar -C /usr --strip-components=1 -xzvf GNULinux_binaries_v3.4.0.tar.gz && \
	rm -rf /var/cache/apt/* && \
	rm -rf /var/lib/apt/lists/* && \
	rm *.deb && \
	rm *.tar.gz && \
	useradd -m -s /bin/bash user

USER user
WORKDIR /home/user

ENTRYPOINT ["/usr/bin/pw-jack", "/usr/bin/ardour"]
